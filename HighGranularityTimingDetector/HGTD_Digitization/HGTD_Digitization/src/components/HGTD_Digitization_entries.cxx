#include "HGTD_Digitization/HGTD_DigitizationTool.h"
#include "HGTD_Digitization/HGTD_FrontEndTool.h"
#include "HGTD_Digitization/HGTD_SurfaceChargesGenerator.h"
#include "HGTD_Digitization/HGTD_TimingResolution.h"

DECLARE_COMPONENT(HGTD_DigitizationTool)
DECLARE_COMPONENT(HGTD_FrontEndTool)
DECLARE_COMPONENT(HGTD_SurfaceChargesGenerator)
DECLARE_COMPONENT(HGTD_TimingResolution)
